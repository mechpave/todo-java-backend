ALTER TABLE `todo_items` ADD `user_id` INT UNSIGNED NOT NULL;
ALTER TABLE `todo_items` ADD CONSTRAINT fk_todo_items_users FOREIGN KEY (`user_id`) REFERENCES `users`(`id`);