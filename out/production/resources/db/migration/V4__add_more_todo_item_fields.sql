ALTER TABLE `todo_items`
    ADD `completed` TINYINT(1) NOT NULL DEFAULT 0 AFTER `title`,
    ADD `created_at` DATETIME NOT NULL DEFAULT NOW(),
    ADD `deadline` DATE NULL
;