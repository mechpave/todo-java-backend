FROM openjdk:8-jdk-alpine
WORKDIR /workspace/app

RUN apk --no-cache add bash

COPY ./ ./

VOLUME /tmp

ENTRYPOINT ["java","-jar","build/libs/todo-app-0.1.0.jar"]
