API backend for TODO list project
=================================

This repository contains back-end part for TODO list project.

Note that user passwords are stored in plain text and authentication method is HTTP basic (For demo purposes).
There is no gradle script for building docker image. I was unable to do that, so compiled jar is committed to VCS
and used by docker image.

# Build instructions

1. Run `docker-compose up`
2. Checkout and build front-end part (https://bitbucket.org/mechpave/todo-webapp/src/master/)