package todo.infrastructure.database.mapper;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.springframework.stereotype.Component;
import todo.domain.model.TodoItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class JdbiTodoItemMapper implements RowMapper<TodoItem> {

    @Override
    public TodoItem map(ResultSet rs, StatementContext ctx) throws SQLException {

        LocalDate deadline = null;
        LocalDateTime createdAt = null;

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        if (rs.getString("deadline") != null) {
            deadline = LocalDate.parse(rs.getString("deadline"), dateFormatter);
        }

        if (rs.getString("createdAt") != null) {
            createdAt = LocalDateTime.parse(rs.getString("createdAt"), dateTimeFormatter);
        }

        return new TodoItem(
                rs.getInt("id"),
                rs.getString("title"),
                rs.getInt("user_id"),
                rs.getBoolean("completed"),
                deadline,
                createdAt
        );
    }
}
