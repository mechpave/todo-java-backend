package todo.infrastructure.database.mapper;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.springframework.stereotype.Component;
import todo.domain.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class JdbiUserMapper implements RowMapper<User> {

    @Override
    public User map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new User(
                rs.getInt("id"),
                rs.getString("email"),
                rs.getString("password")
        );
    }
}
