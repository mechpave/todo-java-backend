package todo.infrastructure.database.repository;

import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import todo.domain.model.User;
import todo.domain.model.UserRepository;
import todo.infrastructure.database.mapper.JdbiUserMapper;

import javax.sql.DataSource;
import java.util.Optional;

@Component
public class JdbiUserRepository implements UserRepository {

    @Qualifier("dataSource")
    private DataSource dataSource;

    private JdbiUserMapper mapper;

    @Autowired
    public JdbiUserRepository(DataSource dataSource, JdbiUserMapper mapper) {
        this.dataSource = dataSource;
        this.mapper = mapper;
    }

    public User findByEmail(String email) {
        String query = "SELECT id, email, password FROM users WHERE email = :email";

        Jdbi jdbi = Jdbi.create(dataSource);
        Optional<User> user = jdbi.withHandle(handle -> handle.createQuery(query)
                .bind("email", email)
                .map(mapper)
                .findFirst()
        );

        return user.orElse(null);
    }
}
