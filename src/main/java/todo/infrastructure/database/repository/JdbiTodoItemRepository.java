package todo.infrastructure.database.repository;

import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import todo.domain.model.TodoItem;
import todo.domain.model.TodoItemRepository;
import todo.infrastructure.database.mapper.JdbiTodoItemMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Component
public class JdbiTodoItemRepository implements TodoItemRepository {

    @Qualifier("dataSource")
    private DataSource dataSource;

    private JdbiTodoItemMapper mapper;

    @Autowired
    public JdbiTodoItemRepository(DataSource dataSource, JdbiTodoItemMapper mapper) {
        this.dataSource = dataSource;
        this.mapper = mapper;
    }

    @Override
    public TodoItem findOneByIdAndUserId(Integer id, Integer userId) {
        Jdbi jdbi = Jdbi.create(dataSource);

        String query =
                "SELECT " +
                        "item.id AS id, " +
                        "item.title AS title, " +
                        "item.completed AS completed, " +
                        "item.deadline AS deadline, " +
                        "item.created_at AS createdAt, " +
                        "item.user_id as user_id " +
                        "FROM todo_items item " +
                        "LEFT JOIN users user ON user.id = item.user_id " +
                        "WHERE item.id = :id " +
                        "AND user.id = :userId " +
                        "LIMIT 1"
                ;

        Optional<TodoItem> result = jdbi.withHandle(handle -> handle.createQuery(query)
                .bind("id", id)
                .bind("userId", userId)
                .map(mapper)
                .findFirst()
        );

        return result.orElse(null);
    }

    @Override
    public List<TodoItem> findAllByUserId(Integer userId) {
        Jdbi jdbi = Jdbi.create(dataSource);

        String query =
                "SELECT " +
                    "item.id AS id, " +
                    "item.title AS title, " +
                    "item.completed AS completed, " +
                    "item.deadline AS deadline, " +
                    "item.created_at AS createdAt, " +
                    "item.user_id as user_id " +
                "FROM todo_items item " +
                "LEFT JOIN users user ON user.id = item.user_id " +
                "WHERE user.id = :userId " +
                "ORDER BY item.completed ASC, (item.deadline IS NULL), item.deadline ASC, item.id DESC";

        return jdbi.withHandle(handle -> handle.createQuery(query)
                .bind("userId", userId)
                .map(mapper)
                .list()
        );
    }

    @Override
    public void save(TodoItem item) {
        if (item.getId() == null) {
            insert(item);
        } else {
            update(item);
        }
    }

    private void insert(TodoItem item) {
        Jdbi jdbi = Jdbi.create(dataSource);

        String query = "INSERT INTO todo_items (title, deadline, user_id) VALUES (:title, :deadline, :userId)";

        jdbi.useHandle(handle -> {
            Integer insertedId = handle.createUpdate(query)
                    .bind("title", item.getTitle())
                    .bind("deadline", item.getDeadline())
                    .bind("userId", item.getUserId())
                    .executeAndReturnGeneratedKeys()
                    .mapTo(Integer.class)
                    .findOnly();

            item.setId(insertedId);
        });
    }

    private void update(TodoItem item) {
        Jdbi jdbi = Jdbi.create(dataSource);

        String query = "UPDATE todo_items SET " +
                "title = :title, " +
                "deadline = :deadline, " +
                "completed = :completed " +
                "WHERE id = :id";

        jdbi.useHandle(handle -> {
                    handle.createUpdate(query)
                            .bind("title", item.getTitle())
                            .bind("deadline", item.getDeadline())
                            .bind("completed", item.isCompleted())
                            .bind("id", item.getId())
                            .execute();
                }
        );
    }
}
