package todo.application.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import todo.domain.model.User;
import todo.domain.model.UserRepository;

import java.util.Collections;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    private UserRepository repository;

    @Autowired
    public UserAuthenticationProvider(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        User user = repository.findByEmail(name);
        if (user == null) {
            throw new BadCredentialsException("User does not exist");
        }

        // TODO: Set up password encryption.
        if (!user.getPassword().equals(password)) {
            throw new BadCredentialsException("Authentication failed");
        }

        return new UsernamePasswordAuthenticationToken(
                user.getEmail(),
                user.getPassword(),
                Collections.emptyList()
        );
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
