package todo.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import todo.application.exception.BadRequestException;
import todo.application.exception.NotFoundException;
import todo.application.UserProvider;
import todo.domain.model.TodoItem;
import todo.domain.model.TodoItemRepository;
import todo.domain.model.User;
import todo.domain.TodoService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@RestController
public class TodoItemController {

    private TodoService todoService;

    private UserProvider userProvider;

    private TodoItemRepository todoItemRepository;

    @Autowired
    public TodoItemController(
            TodoService todoService,
            UserProvider userProvider,
            TodoItemRepository todoItemRepository
    ) {
        this.todoService = todoService;
        this.userProvider = userProvider;
        this.todoItemRepository = todoItemRepository;
    }

    @GetMapping("/api/todo-items")
    public List<TodoItem> getItems() {
        User user = userProvider.getLoggedInUser();

        return todoService.getTodoItemsForUser(user);
    }

    @PostMapping("/api/todo-items")
    public TodoItem create(@RequestBody Map<String,Object> body) {
        User user = userProvider.getLoggedInUser();

        if (body.get("title") == null || body.get("title").toString().equals("")) {
            throw new BadRequestException();
        }

        LocalDate deadline = null;
        if (body.get("deadline") != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            deadline = LocalDate.parse(body.get("deadline").toString(), formatter);
        }

        return todoService.createTodoItemForUser(body.get("title").toString(), deadline, user);
    }

    @PatchMapping("/api/todo-items/{id}/toggle")
    public TodoItem complete(@PathVariable("id") Integer id)
    {
        User user = userProvider.getLoggedInUser();

        TodoItem item = todoService.getTodoItemForUser(id, user);
        if (item == null) {
            throw new NotFoundException();
        }

        item.toggle();
        todoItemRepository.save(item);

        return item;
    }
}
