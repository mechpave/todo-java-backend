package todo.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import todo.domain.model.User;
import todo.domain.model.UserRepository;

@Component
public class UserProvider {

    private UserRepository repository;

    @Autowired
    public UserProvider(UserRepository repository) {
        this.repository = repository;
    }

    public User getLoggedInUser()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = repository.findByEmail(auth.getName());

        return user;
    }
}
