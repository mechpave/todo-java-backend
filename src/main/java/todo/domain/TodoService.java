package todo.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import todo.domain.model.TodoItem;
import todo.domain.model.TodoItemRepository;
import todo.domain.model.User;

import java.time.LocalDate;
import java.util.List;

@Component
public class TodoService {

    private TodoItemRepository repository;

    @Autowired
    public TodoService(TodoItemRepository repository) {
        this.repository = repository;
    }

    public TodoItem getTodoItemForUser(Integer id, User user) {
        return repository.findOneByIdAndUserId(id, user.getId());
    }

    public List<TodoItem> getTodoItemsForUser(User user) {
        return repository.findAllByUserId(user.getId());
    }

    public TodoItem createTodoItemForUser(String title, LocalDate deadline, User user) {
        TodoItem item = new TodoItem(title, deadline, user.getId());
        repository.save(item);

        return item;
    }
}
