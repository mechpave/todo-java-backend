package todo.domain.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TodoItem {

    private Integer id;

    private Integer userId;

    private String title;

    private Boolean completed = false;

    private LocalDate deadline;

    private LocalDateTime createdAt;

    public TodoItem(
            Integer id,
            String title,
            Integer userId,
            Boolean completed,
            LocalDate deadline,
            LocalDateTime createdAt
    ) {
        this.id = id;
        this.title = title;
        this.userId = userId;
        this.completed = completed;
        this.deadline = deadline;
        this.createdAt = createdAt;
    }

    public TodoItem(String title, LocalDate deadline, Integer userId) {
        this.title = title;
        this.deadline = deadline;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getUserId() {
        return userId;
    }

    public void toggle() {
        completed = !completed;
    }

    public Boolean isCompleted() {
        return completed;
    }

    public String getDeadline() {
        if (deadline != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            return deadline.format(formatter);
        }

        return null;
    }

    public String getCreatedAt() {
        if (createdAt != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return createdAt.format(formatter);
        }

        return null;
    }
}
