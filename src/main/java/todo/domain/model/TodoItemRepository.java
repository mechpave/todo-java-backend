package todo.domain.model;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public interface TodoItemRepository {

    TodoItem findOneByIdAndUserId(Integer id, Integer userId);

    List<TodoItem> findAllByUserId(Integer userId);

    void save(TodoItem item);
}
